#------------------------------------------------------
# tests the basic functionality of from_VTK 
# 
#
# fada 19
#------------------------------------------------------


import sys
sys.path.append('/home/fadanelli/fibo_beta') 

import fibo_beta as fb



#======regulate=parameters=======================================================
data_address = '/work2/manuela/Run_mms_real_tris/10' 
fibo_name = 'mmr3' 
#================================================================================

#======regulate=parameters=======================================================
#data_address = '/work2/fadanelli/_eq4d/data_para/'
#fibo_name = 'eq4d'
#================================================================================







#----create-your-objects-----------
alldata = fb.fibo(fibo_name)
from_VTK = fb.from_VTK(data_address)


#----load-everything-------------------
if fibo_name == 'mmr3' :
	from_VTK.get_scal(tar_file='para_TRAC_450_red',fibo_obj=alldata,tar_var='Trac',double_y=True)
	from_VTK.get_vect(tar_file='para_B_450_red',fibo_obj=alldata,tar_var='B',double_y=True)

if fibo_name == 'eq4d' :
	from_VTK.get_scal(tar_filee='eq4d_Trac_0460_red',fibo_obj=alldata,double_y=True)
	from_VTK.get_vect(tar_file='eq4d_B_0460_red',fibo_obj=alldata,double_y=True)

alldata.meta = from_VTK.meta

