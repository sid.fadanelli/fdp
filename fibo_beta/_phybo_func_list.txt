fibo_beta1
------------------------------  



phybo(fibo_obj): 
-----------------------------------------------------------------------
-----------------------------------------------------------------------

- calc_ref_change     calculates the reference frame with zero single-fluid velocity at a certain point

- calc_Psi            calculates the magnetic flux function (only 2D periodic slices ..)
- calc_ED_decs        calculates the par ((and per)) components in all Ks and all Us
- calc_energy_conv    calculates all energy conversion rates


